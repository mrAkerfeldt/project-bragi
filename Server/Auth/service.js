// services.js
var jwt = require('jwt-simple');
var moment = require('moment');
var config = require('./../db/config.json');

module.exports.createToken = function(user, admin) {

    var payload = {
        sub: user,
        admin: admin,
        iat: moment().unix(),
        exp: moment().add(6, "days").unix(),
    };

    return jwt.encode(payload, config.secret.token);
};