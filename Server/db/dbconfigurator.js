const {Pool} = require('pg');
var dbconfigs = require('./config.json');


const pool = new Pool({
    user: dbconfigs.postgres.user,
    host: dbconfigs.postgres.host,
    database: dbconfigs.postgres.database,
    password: dbconfigs.postgres.password,
    port: dbconfigs.postgres.port,
})

module.exports = {
    query: (text, params, callback) => {
        const start = Date.now()
        return pool.query(text, params, (err, res) => {
            const duration = Date.now() - start
            console.log("\n" + "executed query", { text, duration, params}, "\n")
            callback(err, res)
        })
    }
}