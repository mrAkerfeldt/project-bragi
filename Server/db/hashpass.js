const bcrypt = require('bcrypt-nodejs');
var dbconfigs = require('./config.json');


module.exports.hashPassword = (password) =>{
        var hash = bcrypt.hashSync(password, dbconfigs.hashpassword.salt)
        return hash;
}

module.exports.compareHash = (password, hash) =>{
        var compare = bcrypt.compareSync(password, hash)
        return compare;
}