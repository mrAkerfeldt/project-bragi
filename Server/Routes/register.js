module.exports = function (app) {

    const db = require('../db/dbconfigurator.js');
    var hashpass = require('../db/hashpass.js')
    var queries = require('../db/queries.json');

    var response;


        app.post("/register", function (req, res) {

            const values = [req.body.telefono, req.body.nickname, hashpass.hashPassword(req.body.password), req.body.email, req.body.nombre, false];

                db.query(queries.register.register, values, (err, result) => {
                    if (err) {
                        console.log(err);
                        return res
                            .status(418)
                            .send('Ha habido un problema con su solicitud');
                    } else{
                        return res
                            .status(200)
                            .send('Registro exitoso');
                    }
                })

        })
}