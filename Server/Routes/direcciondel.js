//Giving express access to route.
module.exports = function (app) {

    const db = require('./../db/dbconfigurator');
    const queries = require('./../db/queries.json');
    const middleware = require('./../Auth/middleware');

    var params;

    app.post("/direcciondel",middleware.isAuthenticated, function (req, res) {

        if(req.user == req.body.usuario){
            deldireccion(req, res)

        } else {
            if(req.admin == true){
                deldireccion(req, res)

            } else {
                return res
                    .status(418)
                    .send("No tienes permisos para realizar esta operacion");

            }

        }

    });

    function deldireccion(req, res) {

        params = [req.body.usuario, req.body.valor];

        db.query(queries.direcciones.delete, [params], (err, result) => {
            if (err) {
                console.log(err);
                res.send(418);

            } else {
                return res
                    .status(200)
            }
        });
    }

}