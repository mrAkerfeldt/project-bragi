//Giving express access to route.
module.exports = function (app) {

    const db = require('./../db/dbconfigurator');
    const queries = require('./../db/queries.json');
    const middleware = require('./../Auth/middleware');

    var params;

    app.post("/cuentasadmindel",middleware.isAuthenticated, function (req, res) {

        if(req.admin == true){

            delcuentas(req, res)

        } else {
            return res
                .status(418)
                .send("No tienes permisos para realizar esta operacion");
        }
    });

    function delcuentas(req, res) {

        params = [req.valor];

        db.query(queries.cuentas.delete, [params], (err, result) => {
            if (err) {
                console.log(err);
                res.send(418);

            } else {
                return res
                    .status(200)
            }
        });

    }
}