//Giving express access to route.
module.exports = function (app) {

    const db = require('./../db/dbconfigurator');
    const queries = require('./../db/queries.json');
    const middleware = require('./../Auth/middleware');

    var params;

    app.post("/direccion",middleware.isAuthenticated, function (req, res) {

        params = [req.body.nombre, req.user, req.body.direccion, req.body.pais, req.body.ciudad, req.estado];

        direcciones(queries.direcciones.agregar, params, res)

    });

    app.get("/direccion", middleware.isAuthenticated, function (req, res) {

        switch(req.query.dato){

            case "BLANK": {

                if(req.admin == true) {
                    direcciones(queries.direcciones.getall, null, res);

                } else{
                    return res
                        .status(418)
                        .send('No tienes permisos para realizar esta operacion')

                }

            }

            case "nickname":{

                if(req.user == req.query.valor){
                    direcciones(queries.direcciones.getnickname, [req.query.valor], res)

                } else if(req.admin == true){
                    direcciones(queries.usuarios.getnickname, [req.query.valor], res);

                } else{
                    return res
                        .status(418)
                        .send('No tienes permisos para realizar esta operacion')

                }
                break;

            }


            default: {
                return res
                    .status(418)
                    .send('Parametro no valido')
            }

        }

    })

    function direcciones(query, parameters, res) {

        if(parameters == null) {
            db.query(query, (err, result) => {
                if (err) {
                    console.log(err);
                    return res
                        .status(418)
                } else {
                    return res
                        .status(200)
                        .send(result.rows);
                }
            });

        } else {

            db.query(query, parameters, (err, result) => {
                if (err) {
                    console.log(err);
                    return res
                        .status(418)
                } else {
                    return res
                        .status(200)
                        .send(result.rows);
                }
            });
        }
    }
}