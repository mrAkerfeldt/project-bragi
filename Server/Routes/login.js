//Giving express access to route.
module.exports = function (app) {

    const db = require('./../db/dbconfigurator');
    const hashpass = require('./../db/hashpass');
    const queries = require('./../db/queries.json');
    var service = require('./../Auth/service');

    var data;
    var admin;

    app.post("/login", function (req, res) {

        db.query(queries.usuarios.loginusers, [req.body.nickname], (err, result) => {
            if (err) {
                console.log(err);
                data = false;

            } else{

                data = hashpass.compareHash(req.body.password, result.rows[0].password_user);

                if(result.rows[0].admin_user == true){
                    admin = true;
                } else{
                    admin = false;
                }

                if (data){
                    return res
                        .status(200)
                        .send({token: service.createToken(req.body.nickname, admin)});
                } else {
                    return res
                        .status(418)
                        .send('Clave incorrecta');
                }
            }
        });
    });
}