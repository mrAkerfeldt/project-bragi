module.exports = function (app) {

    const   formidable = require('formidable'),
            util = require('util'),
            fs = require('fs'),
            db = require('./../db/dbconfigurator'),
            queries = require('./../db/queries.json'),
            middleware = require('./../Auth/middleware');

    app.post("/productos",middleware.isAuthenticated, function (req, res) {



    });

    app.get("/productos", function (req, res) {

        switch(req.query.dato){

            case "BLANK": {
                productos(queries.categorias.getall, null, res)
                break;

            }

            case "categoria":{
                productos(queries.categorias.getcategoria, [req.query.valor], res);
                break;

            }

            case "nombre":{
                productos(queries.categorias.getcategoria, [req.query.valor], res)
                break;

            }


            default: {
                return res
                    .status(418)
                    .send('Parametro no valido')
                break;
            }

        }

    })

    function productos(query, parameters, res) {

        if(parameters == null) {
            db.query(query, (err, result) => {
                if (err) {
                    console.log(err);
                    return res
                        .status(418)
                } else {
                    return res
                        .status(200)
                        .send(result.rows);
                }
            });

        } else {
            db.query(query, parameters, (err, result) => {
                if (err) {
                    console.log(err);
                    return res
                        .status(418)
                } else {
                    return res
                        .status(200)
                        .send(result.rows);
                }
            });
        }
    }

}