//Giving express access to route.
module.exports = function (app) {

    const db = require('./../db/dbconfigurator');
    const queries = require('./../db/queries.json');
    const middleware = require('./../Auth/middleware');

    var params;

    app.post("/cuentasadmin",middleware.isAuthenticated, function (req, res) {

        if(req.admin == true){

            params = [req.body.nombre, req.body.nickname, req.body.banco, req.body.numero, req.body.email, req.body.identificacion];

            categorias(queries.cuentas.agregar, params, res);

        } else {
            return res
                .status(418)
                .send('No tienes permisos para realizar esta accion')
        }

    });

    app.get("/cuentasadmin", middleware.isAuthenticated, function (req, res) {

        switch(req.query.dato){

            case "BLANK": {

                    categorias(queries.cuentas.getall, null, res)

                break;
            }

            case "nombre":{

                if(req.admin == true) {
                    categorias(queries.cuentas.getnombre, [req.query.valor], res);

                } else{
                    return res
                        .status(418)
                        .send('No tienes permisos para realizar esta operacion')

                }
                break;
            }

            default: {
                return res
                    .status(418)
                    .send('Parametro no valido')

                break;
            }
        }
    })

    function categorias(query, parameters, res) {

        if(parameters == null) {
            db.query(query, (err, result) => {
                if (err) {
                    console.log(err);
                    return res
                        .status(418)
                } else {
                    return res
                        .status(200)
                        .send(result.rows);
                }
            });

        } else {

            db.query(query, parameters, (err, result) => {
                if (err) {
                    console.log(err);
                    return res
                        .status(418)
                } else {
                    return res
                        .status(200)
                        .send(result.rows);
                }
            });
        }
    }
}
