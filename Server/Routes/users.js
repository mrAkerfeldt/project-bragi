//Giving express access to route.
module.exports = function (app) {

    const db = require('./../db/dbconfigurator');
    const queries = require('./../db/queries.json');
    const middleware = require('./../Auth/middleware');
    const hasspass = require('./../db/hashpass');

    var params;

    app.post("/users",middleware.isAuthenticated, function (req, res) {

        if(req.admin == true){

            admin = req.body.admin;

        } else{

            admin = false;

        }
            params = [req.body.telefono, hasspass.hashPassword(req.body.password), req.body.email, req.body.nombre, admin,  req.user];

            db.query(queries.usuarios.alter, [params], (err, result) => {
                if (err) {
                    console.log(err);
                    res.send(418);

                } else {
                    return res
                        .status(200)
                        .send('Solicitud realizada con exito');

                }
            });

    });

    app.get("/users", middleware.isAuthenticated, function (req, res) {

        switch(req.query.dato){

            case "nombre": {

                if (req.admin == true) {
                    users(queries.usuarios.getnombre, [req.query.valor], res);

                } else {
                    return res
                        .status(418)
                        .send('No tienes permisos para realizar esta operacion')
                }
                break;

            }

            case "nickname":{

                if(req.user == req.query.valor){
                    users(queries.usuarios.getnickname, [req.user], res);

                } else if (req.admin == true) {
                    users(queries.usuarios.getnickname, [req.query.valor], res);

                } else {
                    return res
                        .status(418)
                        .send('No tienes permisos para realizar esta operacion')
                }

                break;

            }

            case "telefono": {

                if(req.admin == true) {
                    users(queries.usuarios.gettelefono, [req.query.valor], res)

                } else {
                    return res
                        .status(418)
                        .send('No tienes permisos para realizar esta operacion')
                }

                break;

            }

            case "email": {

                if (req.admin == true) {
                    users(queries.usuarios.getemail, [req.query.valor], res);

                } else {
                    return res
                        .status(418)
                        .send('No tienes permisos para realizar esta operacion')
                }
                break;

            }

            case "BLANK":{

                if(req.admin == true){

                    users(queries.usuarios.getall, null, res)

                } else {
                    return res
                        .status(418)
                        .send('No tienes permisos para realizar esta operacion')
                }

                break;

            }

            default: {
                return res
                    .status(418)
                    .send('Parametro no valido')
            }

        }

    })

    function users(query, parameters, res) {

        if(parameters == null){

            db.query(queries.usuarios.getall, (err, result) => {
                if (err) {
                    console.log(err);
                    return res
                        .status(418)
                } else {
                    return res
                        .status(200)
                        .send(result.rows);

                }
            });

         } else {

            console.log('not here')

            db.query(query, parameters, (err, result) => {
                if (err) {
                    console.log(err);
                    return res
                        .status(418)
                } else {
                    return res
                        .status(200)
                        .send(result.rows);
                }
            });

        }
    }

}