//Giving express access to route.
module.exports = function (app) {

    const db = require('./../db/dbconfigurator');
    const queries = require('./../db/queries.json');
    const middleware = require('./../Auth/middleware');

    var params;

    app.post("/categoriasdel",middleware.isAuthenticated, function (req, res) {

        if(req.admin == true){

            delcategorias(req, res)

        } else {
            return res
                .status(418)
                .send("No tienes permisos para realizar esta operacion");
        }
    });

    function delcategorias(req, res) {

        params = [req.valor];

                    db.query(queries.direcciones.delete, [params], (err, result) => {
                        if (err) {
                            console.log(err);
                            res.send(418);

                        } else {
                            return res
                                .status(200)
                        }
                    });

        }
}