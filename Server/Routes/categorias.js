//Giving express access to route.
module.exports = function (app) {

    const db = require('./../db/dbconfigurator');
    const queries = require('./../db/queries.json');
    const middleware = require('./../Auth/middleware');

    app.post("/categorias",middleware.isAuthenticated, function (req, res) {

        if(req.admin == true){
            categorias(queries.categorias.agregar, req.body.valor, res)

        } else {
            return res
                .status(418)
                .send('No tienes permisos para realizar esta accion')
        }

    });

    app.get("/categorias", middleware.isAuthenticated, function (req, res) {

        switch(req.query.dato){

            case "BLANK": {

                if(req.admin == true){

                    categorias(queries.categorias.getall, null, res)

                } else {
                    return res
                        .status(418)
                        .send('No tienes permisos para realizar esta operacion')
                }
                break;

            }

            case "categoria":{

                categorias(queries.categorias.getcategoria, [req.query.valor], res);
                break;
            }


            default: {
                return res
                    .status(418)
                    .send('Parametro no valido')
                break;
            }

        }

    })

    function categorias(query, parameters, res) {

        if(parameters == null) {
            db.query(query, (err, result) => {
                if (err) {
                    console.log(err);
                    return res
                        .status(418)
                } else {
                    return res
                        .status(200)
                        .send(result.rows);
                }
            });

        } else {

            db.query(query, parameters, (err, result) => {
                if (err) {
                    console.log(err);
                    return res
                        .status(418)
                } else {
                    return res
                        .status(200)
                        .send(result.rows);
                }
            });
        }
    }
}

