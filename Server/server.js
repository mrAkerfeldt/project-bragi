//modules
const express = require('express');
const path = require("path");
const bodyParser = require('body-parser');
const multer = require('multer');

/*
Note: For the config file, USE JSON.parse, everytime you have to call it.
If you use Require, you will be returned a cached copy. So if the JSON is updating itself, it is not gonna work.

var fs = require('fs');
var json = JSON.parse(fs.readFileSync('/path/to/file.json', 'utf8'));

Use that for the Config of the app.

NOT REQUIRE.
*/

//setting express
var app = express();
app.set("port", process.env.PORT || 9390);
app.use(express.static(__dirname + '/public'));

//The use of the bodyParser constructor (app.use(bodyParser());) has been deprecated
//Now is a middleware, so you have to call the methods separately...
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

//Allow CROSS-ORIGINS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', "POST, GET, PUT, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//routes
const login = require('./routes/login');
const preguntas = require('./routes/preguntas');
const productos = require('./routes/productos');
const productosdel = require('./routes/productosdel');
const register = require('./routes/register');
const categorias = require ('./routes/categorias');
const categoriasdel = require('./routes/categoriasdel');
const compras = require('./routes/compras');
const cuentasadmin = require('./routes/cuentasadmin');
const cuentasadmindel = require('./routes/cuentasadmindel');
const direccion = require('./routes/direccion');
const direcciondel = require('./routes/direcciondel');
const users = require('./routes/users');

//giving express access to routes
login(app);
preguntas(app);
productos(app);
productosdel(app);
register(app);
categorias(app);
categoriasdel(app);
compras(app);
cuentasadmindel(app);
cuentasadmin(app);
direccion(app);
direcciondel(app);
users(app);

//basic redirect
app.get('/', function (req, res) {
    res.redirect('public/index.html');
});

//start the server
app.listen(app.get('port'), function(){
    console.log('Express server () listening on localhost:' + app.get('port'));
});

var hasspass = require('./db/hashpass');

console.log (hasspass.hashPassword('masterkey'));